# Domain设计

下面将介绍Video的表设计，和模型定义。

## 表设计

### `Videos`设计

```csharp

/// <summary>
/// 视频聚合
/// </summary>
public class Video : FullAggregateRoot<long, long>
{
    /// <summary>
    /// 视频标题
    /// </summary>
    public string title;

    /// <summary>
    /// 视频描述
    /// </summary>
    public string description;

    /// <summary>
    /// 视频地址
    /// </summary>
    public string url;

    /// <summary>
    /// 视频总长度（秒）
    /// </summary>
    public int duration;

    /// <summary>
    /// 浏览量
    /// </summary>
    public int views;

    /// <summary>
    /// 视频标签
    /// </summary>
    public List<string> tags;

    /// <summary>
    /// 视频封面图片地址
    /// </summary>
    public string thumbnailUrl;

    /// <summary>
    /// 视频播放次数
    /// </summary>
    public long playCount;

    /// <summary>
    /// 视频点赞数
    /// </summary>
    public long likes;

    /// <summary>
    /// 视频踩数
    /// </summary>
    public long dislikes;

    /// <summary>
    /// 视频评论数
    /// </summary>
    public long commentsCount;

    /// <summary>
    /// 视频标题
    /// </summary>
    public string Title { get => title; set => title = value; }

    /// <summary>
    /// 视频描述
    /// </summary>
    public string Description { get => description; set => description = value; }

    /// <summary>
    /// 视频地址
    /// </summary>
    public string Url { get => url; set => url = value; }

    /// <summary>
    /// 视频总长度（秒）
    /// </summary>
    public int Duration { get => duration; private set => duration = value; }

    /// <summary>
    /// 浏览量
    /// </summary>
    public int Views { get => views; private set => views = value; }

    /// <summary>
    /// 视频标签
    /// </summary>
    public List<string> Tags { get => tags; set => tags = value; }

    /// <summary>
    /// 视频封面图片地址
    /// </summary>
    public string ThumbnailUrl { get => thumbnailUrl; set => thumbnailUrl = value; }

    /// <summary>
    /// 视频播放次数
    /// </summary>
    public long PlayCount { get => playCount; private set => playCount = value; }

    /// <summary>
    /// 视频点赞数
    /// </summary>
    public long Likes { get => likes; private set => likes = value; }

    /// <summary>
    /// 视频踩数
    /// </summary>
    public long Dislikes { get => dislikes; private set => dislikes = value; }

    /// <summary>
    /// 视频评论数
    /// </summary>
    public long CommentsCount { get => commentsCount; private set => commentsCount = value; }

    protected Video()
    {
    }

    public Video(long id) : base(id)
    {
    }

    /// <summary>
    /// 修改视频总长度
    /// </summary>
    /// <param name="duration"></param>
    public void SetDuration(int duration)
    {
        Duration = duration;
    }
}
```

### `Users`设计

```csharp

/// <summary>
/// 用户表
/// </summary>
public class VideoUsers : FullAggregateRoot<long, long>
{
    /// <summary>
    /// 用户名
    /// </summary>
    private string username;

    /// <summary>
    /// 用户密码
    /// </summary>
    private string password;

    /// <summary>
    /// 用户邮箱
    /// </summary>
    private string email;

    /// <summary>
    /// 用户头像
    /// </summary>
    private string avatar;

    /// <summary>
    /// 用户描述
    /// </summary>
    private string description;

    /// <summary>
    /// 用户名
    /// </summary>
    public string Username { get => username; private set => username = value; }

    /// <summary>
    /// 用户密码
    /// </summary>
    public string Password { get => password; private set => password = value; }

    /// <summary>
    /// 用户邮箱
    /// </summary>
    public string Email { get => email; private set => email = value; }

    /// <summary>
    /// 用户头像
    /// </summary>
    public string Avatar { get => avatar; set => avatar = value; }

    /// <summary>
    /// 用户描述
    /// </summary>
    public string Description { get => description; set => description = value; }

    protected VideoUsers()
    {

    }

    public VideoUsers(string username, string password, string email, string avatar, string description)
    {
        SetUserName(username);
        UpdateEmail(email);
        UpdatePassword(password);
        Avatar = avatar;
        Description = description;
    }

    /// <summary>
    /// 修改用户名
    /// </summary>
    /// <param name="username"></param>
    /// <exception cref="UserFriendlyException"></exception>
    public void SetUserName(string username)
    {
        // 判断用户名是否符合要求
        if (username.IsNullOrWhiteSpace())
        {
            throw new UserFriendlyException("用户名不能为空");
        }

        if (username.Length < 5)
        {
            throw new UserFriendlyException("用户名长度不能小于5位");
        }

        Username = username;
    }

    /// <summary>
    /// 修改密码
    /// </summary>
    /// <param name="password"></param>
    /// <exception cref="UserFriendlyException"></exception>
    public void UpdatePassword(string password)
    {
        // 判断密码是否符合要求
        if (password.IsNullOrWhiteSpace())
        {
            throw new UserFriendlyException("密码不能为空");
        }

        if (!IsComplexPassword(password))
        {
            throw new UserFriendlyException("密码过于简单");
        }

        Password = password;
    }

    /// <summary>
    /// 修改邮箱
    /// </summary>
    /// <param name="email"></param>
    /// <exception cref="UserFriendlyException"></exception>
    public void UpdateEmail(string email)
    {
        if (email.IsNullOrWhiteSpace())
        {
            throw new UserFriendlyException("邮箱不能为空");
        }

        if (!IsValidEmail(email))
        {
            throw new UserFriendlyException("邮箱校验失败");
        }

        Email = email;
    }

    public static bool IsValidEmail(string email)
    {
        // 正则表达式模式
        string pattern = @"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$";

        // 使用正则表达式进行匹配
        Match match = Regex.Match(email, pattern);

        // 如果匹配成功，则为有效邮箱
        return match.Success;
    }
    public static bool IsComplexPassword(string password)
    {
        bool hasUpperCase = password.Any(char.IsUpper);
        bool hasLowerCase = password.Any(char.IsLower);
        bool hasDigit = password.Any(char.IsDigit);
        bool hasSpecialChar = password.Any(ch => !char.IsLetterOrDigit(ch));

        return password.Length >= 8 && hasUpperCase && hasLowerCase && hasDigit && hasSpecialChar;
    }
}
```

### `Roles`设计

```csharp

public class Roles : FullAggregateRoot<int, long>
{
    /// <summary>
    /// 角色排序
    /// </summary>
    public int sort;

    /// <summary>
    /// 角色名称
    /// </summary>
    public string name;

    /// <summary>
    /// 角色描述
    /// </summary>
    public string description;

    /// <summary>
    /// 默认角色不允许删除
    /// </summary>
    public bool isDefault;

    /// <summary>
    /// 是否正常使用的
    /// </summary>
    public bool isActive;
    /// <summary>
    /// 角色排序
    /// </summary>
    public int Sort { get => sort; set => sort = value; }

    /// <summary>
    /// 角色名称
    /// </summary>
    public string Name { get => name; set => name = value; }

    /// <summary>
    /// 角色描述
    /// </summary>
    public string Description { get => description; set => description = value; }

    /// <summary>
    /// 默认角色不允许删除
    /// </summary>
    public bool IsDefault { get => isDefault; private set => isDefault = value; }

    /// <summary>
    /// 是否正常使用的
    /// </summary>
    public bool IsActive { get => isActive; private set => isActive = value; }

    public Roles()
    {
    }

    public Roles(int sort, string name, string description, bool isDefault)
    {
        UpdateName(name);
        Sort = sort;
        Description = description;
        IsDefault = isDefault;
        IsActive = true;
    }

    public void UpdateName(string name)
    {
        if (name.IsNullOrWhiteSpace())
        {
            throw new UserFriendlyException("角色名称不能为空");
        }

        Name = name;
    }
}
```

### `RoleUsers`设计

```csharp
public class RoleUsers : Entity<long>
{
    /// <summary>
    /// 用户id
    /// </summary>
    public long UserId { get; set; }

    /// <summary>
    /// 角色id
    /// </summary>
    public long RoleId { get; set; }
}
```

### `VideoComments`设计

```csharp

public class VideoComments : Entity<long>, IAuditEntity<long>
{
    public long videoId;

    public string comment;

    public long? parentId;

    /// <summary>
    /// 视频Id
    /// </summary>
    public long VideoId { get => videoId; private set => videoId = value; }

    /// <summary>
    /// 评论内容
    /// </summary>
    public string Comment { get => comment; private set => comment = value; }

    /// <summary>
    /// 父级id如果为空则是顶层
    /// </summary>
    public long? ParentId { get => parentId; private set => parentId = value; }

    public long Creator { get; private set; }
    public DateTime CreationTime { get; private set; }
    public long Modifier { get; private set; }
    public DateTime ModificationTime { get; private set; }

    protected VideoComments()
    {
    }

    public VideoComments(long videoId, string comment, long? parentId)
    {
        UpdateVideoId(videoId);
        UpdateComment(comment);
        VideoId = videoId;
        ParentId = parentId;
    }

    public void UpdateVideoId(long videoId)
    {
        if (videoId == null)
        {
            throw new UserFriendlyException("您的视频id为空！");
        }

        VideoId = videoId;
    }

    public void UpdateComment(string comment)
    {
        if (comment.IsNullOrWhiteSpace())
        {
            throw new UserFriendlyException("您的评论为空");
        }

        Comment = comment;
    }
}
```

### `VideoLikes`设计

```csharp
public class VideoLikes{
    
    public long Id { get; set; }

    /// <summary>
    /// 视频id
    /// </summary>
    public long VideoId { get; set; }

    /// <summary>
    /// 点赞用户id
    /// </summary>
    public long UserId { get; set; }
}
```

### `VideoCategories`设计

```csharp

public class VideoCategories : Entity<int>
{
    public string name;

    public string description;

    /// <summary>
    /// 分类名称
    /// </summary>
    public string Name { get => name; private set => name = value; }

    /// <summary>
    /// 分类描述
    /// </summary>
    public string Description { get => description; private set => description = value; }

    protected VideoCategories()
    {

    }

    public VideoCategories(string name, string description)
    {
        UpdateName(name);
        UpdateDescription(description);
    }

    public void UpdateName(string name)
    {
        if (name.IsNullOrWhiteSpace())
        {
            throw new UserFriendlyException("分类名称不能为空");
        }

        Name = name;
    }

    public void UpdateDescription(string description)
    {
        if (description.IsNullOrWhiteSpace())
        {
            throw new UserFriendlyException("分类描述不能为空");
        }

        Description = description;
    }
}
```

### `VideoTags`设计

```csharp
public class VideoTags : Entity<int>
{
    public string name;

    public string description;

    /// <summary>
    /// 标签名称
    /// </summary>
    public string Name { get => name; private set => name = value; }

    /// <summary>
    /// 标签描述
    /// </summary>
    public string Description { get => description; private set => description = value; }

    protected VideoTags()
    {

    }

    public VideoTags(string name, string description)
    {
        UpdateName(name);
        UpdateDescription(description);
    }

    public void UpdateName(string name)
    {
        if (name.IsNullOrWhiteSpace())
        {
            throw new UserFriendlyException("分类名称不能为空");
        }

        Name = name;
    }

    public void UpdateDescription(string description)
    {
        if (description.IsNullOrWhiteSpace())
        {
            throw new UserFriendlyException("分类描述不能为空");
        }

        Description = description;
    }
}
```

### `UserFollows`设计

```csharp
public class UserFollows : Entity<long>
{
    /// <summary>
    /// 关注者id
    /// </summary>
    public long FollowerId { get; set; }

    /// <summary>
    /// 被关注者id
    /// </summary>
    public long FollowedId { get; set; }
}
```

### `VideoPlayRecords`设计

```csharp
public class VideoPlayRecords : Entity<long>, IAuditEntity<long>
{
    private long videoId;

    private DateTime playTime;

    /// <summary>
    /// 视频id
    /// </summary>
    public long VideoId { get => videoId; private set => videoId = value; }

    /// <summary>
    /// 播放时间
    /// </summary>
    public DateTime PlayTime { get => playTime; private set => playTime = value; }

    public long Creator { get; private set; }

    public DateTime CreationTime { get; private set; }

    public long Modifier { get; private set; }

    public DateTime ModificationTime { get; private set; }

    public VideoPlayRecords()
    {
    }

    public VideoPlayRecords(long videoId, DateTime playTime)
    {
        VideoId = videoId;
        PlayTime = playTime;
    }
}
```

### `FileStorage`设计

```csharp

public class FileStorage : FullAggregateRoot<long, long>
{
    /// <summary>
    /// 文件名
    /// </summary>
    public string name;

    /// <summary>
    /// 文件路径
    /// </summary>
    public string path;

    public long size;

    public FileType type;

    /// <summary>
    /// 文件名
    /// </summary>
    public string Name { get => name; set => name = value; }

    /// <summary>
    /// 文件路径
    /// </summary>
    public string Path { get => path; set => path = value; }

    /// <summary>
    /// 文件大小
    /// </summary>
    public long Size { get => size; set => size = value; }

    /// <summary>
    /// 文件类型
    /// </summary>
    public FileType Type { get => type; private set => type = value; }

    protected FileStorage()
    {

    }

    public FileStorage(string name, string path, long size, FileType type)
    {
        UpdateName(name);
        UpdatePath(path);
        Size = size;
        Type = type;
    }

    public void UpdateName(string name)
    {
        if (name.IsNullOrWhiteSpace())
        {
            throw new UserFriendlyException("文件名称不能为空");
        }

        Name = name;
    }

    public void UpdatePath(string path)
    {
        if (path.IsNullOrWhiteSpace())
        {
            throw new UserFriendlyException("文件地址不能为空");
        }
        Path = path;
    }
}

// 文件类别
public enum FileType
{
    /// <summary>
    /// 文件
    /// </summary>
    File,

    /// <summary>
    /// 图片
    /// </summary>
    Image,

    /// <summary>
    /// 视频
    /// </summary>
    Video,

    /// <summary>
    /// 音频
    /// </summary>
    Audio
}
```

### `Auditlog`设计

```csharp

/// <summary>
/// 审计模型
/// </summary>
public class Auditlog : FullAggregateRoot<long, long>
{

    /// <summary>
    /// 请求路由
    /// </summary>
    private string requestRoute;

    /// <summary>
    /// 请求路由
    /// </summary>
    public string RequestRoute
    {
        get { return requestRoute; }
        private set { requestRoute = value; }
    }

    /// <summary>
    /// 请求Query参数
    /// </summary>
    private string requestQuery;

    /// <summary>
    ///请求Query参数
    /// </summary>
    public string RequestQuery
    {
        get { return requestQuery; }
        private set { requestQuery = value; }
    }

    /// <summary>
    /// 请求方式
    /// </summary>
    private string requestMethod;

    /// <summary>
    /// 请求方式
    /// </summary>
    public string RequestMethod
    {
        get { return requestMethod; }
        private set { requestMethod = value; }
    }

    /// <summary>
    /// 请求客户端标识
    /// </summary>
    private string clientIdentifier;

    /// <summary>
    /// 请求客户端标识
    /// </summary>
    public string ClientIdentifier
    {
        get { return clientIdentifier; }
        private set { clientIdentifier = value; }
    }

    /// <summary>
    /// 响应状态
    /// </summary>
    private int responseStatus;

    /// <summary>
    /// 响应状态
    /// </summary>
    public int ResponseStatus
    {
        get { return responseStatus; }
        private set { responseStatus = value; }
    }

    /// <summary>
    /// 请求ip
    /// </summary>
    private string requestIp;

    /// <summary>
    /// 请求ip
    /// </summary>
    public string RequestIp
    {
        get { return requestIp; }
        private set { requestIp = value; }
    }

    /// <summary>
    /// 扩展参数
    /// </summary>
    private Dictionary<string, string> extendedParameters;

    /// <summary>
    /// 扩展参数
    /// </summary>
    public Dictionary<string, string> ExtendedParameters
    {
        get { return extendedParameters; }
        private set { extendedParameters = value; }
    }

    /// <summary>
    /// 请求耗时(ms)
    /// </summary>
    private int requestDuration;

    /// <summary>
    /// 请求耗时(ms)
    /// </summary>
    public int RequestDuration
    {
        get => requestDuration;
        private set => requestDuration = value;
    }

    /// <summary>
    /// 应用环境
    /// </summary>
    private string applicationEnvironment;

    /// <summary>
    /// 应用环境
    /// </summary>
    public string ApplicationEnvironment
    {
        get => applicationEnvironment;
        private set => applicationEnvironment = value;
    }

    protected Auditlog()
    {

    }

    public Auditlog(string requestRoute, string requestQuery, string requestMethod, string clientIdentifier, int responseStatus, string requestIp, Dictionary<string, string> extendedParameters, int requestDuration, string applicationEnvironment)
    {
        RequestRoute = requestRoute;
        RequestQuery = requestQuery;
        RequestMethod = requestMethod;
        ClientIdentifier = clientIdentifier;
        ResponseStatus = responseStatus;
        RequestIp = requestIp;
        ExtendedParameters = extendedParameters;
        RequestDuration = requestDuration;
        ApplicationEnvironment = applicationEnvironment;
    }
}
```

### `SettingManager`设计

```csharp
public class SettingManager : Entity<long>
{
    private string name;

    private string value;

    /// <summary>
    /// 扩展参数
    /// </summary>
    public string Name { get => name; private set => name = value; }

    /// <summary>
    /// 请求耗时(ms)
    /// </summary>
    public string Value { get => value; private set => this.value = value; }

    protected SettingManager()
    {

    }

    public SettingManager(string name, string value)
    {
        Name = name;
        Value = value;
    }
}
```
