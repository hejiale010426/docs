# 配置`Video.Service`工作单元

安装一下依赖在`Video.Service`中。

```xml
<PackageReference Include="Masa.Contrib.Ddd.Domain" Version="$(MASAFrameworkVersion)" />
<PackageReference Include="Masa.Contrib.Ddd.Domain.Repository.EFCore" Version="$(MASAFrameworkVersion)" />
<PackageReference Include="Masa.Contrib.Dispatcher.Events" Version="$(MASAFrameworkVersion)" />
<PackageReference Include="Masa.Contrib.Data.UoW.EFCore" Version="$(MASAFrameworkVersion)" />
```

重命名`ExampleDbContext`为`VideoDbContext`

添加一下代码

```csharp
builder.Services.AddDomainEventBus(options =>
    {
        options.UseUoW<VideoDbContext>()
            .UseRepository<VideoDbContext>();
    });
```

配置好工作单元以后就就可以写CURD了。
