# 项目文档

## [Chat文档](./chat/intro)

- 后端
  - [好友关系原理图](./chat/Service/Friend-Chart)
  - [图片压缩](./chat/Service/Image-compression)
  - [获取用户所在城市](./chat/Service/IpOwnership)
  - [群聊实现](./chat/Service/group)
  - [如何使用JWT（JSON Web Token）](./chat/Service/jwt)
  - [SignalR原理讲解](./chat/Service/signalr)

## [Video文档](./video/intro)
